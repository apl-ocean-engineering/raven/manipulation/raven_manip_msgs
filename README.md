[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
# raven_manip_msgs

ROS message definitions for the Raven manipulation project.
